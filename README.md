docker-php
==========

This image is based on **jawira/base** and includes:

- PHP 7.2

Links:

- Bitbucket: https://bitbucket.org/jawira/docker-php/
- Docker Hub: https://hub.docker.com/r/jawira/php/
